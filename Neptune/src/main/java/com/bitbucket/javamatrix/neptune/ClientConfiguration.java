package com.bitbucket.javamatrix.neptune;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class ClientConfiguration
{
    private static Configuration configuration;
    
    //if true, do not load vanilla splashes, only use the custom/mod ones
	public static boolean DO_NOT_LOAD_VANILLA_SPLASHES;

    public static void init(File configFile)
    {
        configuration = new Configuration(configFile);
        
    	Neptune.log.info("Loading client configuration...");

        try
        {
            configuration.load();
            DO_NOT_LOAD_VANILLA_SPLASHES = configuration.get(Configuration.CATEGORY_GENERAL, "dontLoadVanillaSplashes", false, "if true, do not load vanilla splashes, only use the custom/mod ones").getBoolean(false);
            
        }
        catch (Exception e)
        {
        	Neptune.log.error("Neptune had a problem loading its client configuration");
            e.printStackTrace();
        }
        finally
        {
            configuration.save();
        }
    }
    
    
}
