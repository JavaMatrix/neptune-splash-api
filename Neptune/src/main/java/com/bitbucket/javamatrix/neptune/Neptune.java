package com.bitbucket.javamatrix.neptune;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;

import org.apache.commons.io.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.FMLInjectionData;

@Mod(modid = Neptune.modid, version = Neptune.version, name = Neptune.name)
public class Neptune {
	public static final String modid = "neptune";
	public static final String version = "1.7b2";
	public static final String name = "Neptune";
	@Instance(Neptune.modid)
	public static Neptune instance;
	public static List<String> splashes = new ArrayList<String>();
	public static String configFile;
	
	public static Logger log = LogManager.getLogger("Neptune");
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent ev) {
		// Load the name of the suggested config file.
		configFile = ev.getSuggestedConfigurationFile().getAbsolutePath();
		ClientConfiguration.init(ev.getSuggestedConfigurationFile());
	}

	public static void loadSplashes() {
		try {
			
			// A string to load the splashes through.
			String s = "fail";
			
			if(!ClientConfiguration.DO_NOT_LOAD_VANILLA_SPLASHES)
			{
				// Grab a reader for the vanilla splashes.
				BufferedReader vanillaSplashes = new BufferedReader(
						new InputStreamReader(Minecraft
								.getMinecraft()
								.getResourceManager()
								.getResource(
										new ResourceLocation("texts/splashes.txt"))
								.getInputStream(), Charsets.UTF_8));
	
	
				// Loop through every line.
				while ((s = vanillaSplashes.readLine()) != null) {
					// Remove the embarrassing ones.
					if (s.toLowerCase().startsWith("hobo") || s.equalsIgnoreCase("sexy!")) continue;
					
					// Add the good ones.
					splashes.add(s);
				}
				
				// Don't forget to close the reader!
				vanillaSplashes.close();
			}


			// Make a file at config/extra_splashes.txt
			String extraPath = configFile;
			extraPath = extraPath.replace("neptune.cfg", "extra_splashes.txt");
			File f = new File(extraPath);
			
			// Create the file with some good default splashes if it doesn't exist.
			if (!f.exists()) {
				PrintWriter w = new PrintWriter(new BufferedWriter(
						new FileWriter(f)));
				w.println("Splish splash!");
				w.println(EnumChatFormatting.AQUA + "Aqua!");
				w.println("Neptune is the god of splashes!");
				w.println("MOAR SPLASHES!");
				w.println("Peter Johnson!");
				w.println("Smarter than your average bear!");
				w.println("Yes indeed!");
				w.println("Superlative!");
				w.close();
			}

			// Read in the extra splashes the same way we did the Vanilla ones.
			BufferedReader extraSplashes = new BufferedReader(
					new InputStreamReader(new FileInputStream(f)));
			while ((s = extraSplashes.readLine()) != null) {
				splashes.add(s);
			}
			
			// Don't forget to close it!
			extraSplashes.close();
			
			// Some hard-coded credits-style splashes that can't be removed by
			// the user.
			splashes.add("Defenestration Coding!");
			splashes.add("http://defenestrationcoding.wordpress.com/");
			splashes.add("JavaMatrix is awesome!");
			splashes.add("Also try Pluto!");
			splashes.add("Neptune is open source on BitBucket!");
			splashes.add("Ardumedes is TheUltimateArchitect on PMC!");
			splashes.add("Ardu Rox!");
			splashes.add("CosmicDan is out of this world!");
			splashes.add("SynthTones = Win");
			splashes.add("master801 is captain of the Jolly Spider!");
			splashes.add("Glenn's Gases!");
			splashes.add("bsprks is helpful!");
			splashes.add("Also try Equivalent Exchange Reborn!");
			splashes.add("Coming Soon: RandomTech!");
			
			//this adds the current minecraft version
			splashes.add(((String)FMLInjectionData.data()[4]).substring(0, 4) + "!");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// For the use of other modders.
	public static void addSplash(String text) {
		splashes.add(text);
	}

	@EventHandler
	public void init(FMLInitializationEvent ev) {
		// Register the Listener.
		MinecraftForge.EVENT_BUS.register(new GUIListener());
	}
	
	@EventHandler
	public void imcCallback(FMLInterModComms.IMCEvent event)
	{
		// Creates a loop that reads through every message that has been sent to
		// this mod.
		for (final FMLInterModComms.IMCMessage imcMessage : event.getMessages())
		{
			// Checks to see if the message has a specific key.
			if (imcMessage.key.equalsIgnoreCase("add-splash"))
			{
				// Checks to see if a message is a string
				if (imcMessage.isStringMessage())
				{
					Neptune.addSplash(imcMessage.getStringValue());
				}
			}
		}
	}
}
