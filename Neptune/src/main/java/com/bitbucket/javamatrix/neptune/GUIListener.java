package com.bitbucket.javamatrix.neptune;

import java.lang.reflect.Field;
import java.util.Random;

import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class GUIListener {
	
	public String chooseSplash(Random r) {
		// Choose a random splash index and fetch it.  Pretty simple.
		int index = r.nextInt(Neptune.splashes.size());
		return Neptune.splashes.get(index);
	}
	
	@SubscribeEvent
	public void onGuiOpen(GuiOpenEvent ev) {
		// Make sure that this is the main menu.
		if (ev.gui instanceof GuiMainMenu) {
			try {
				// Load up the splashes to avoid an "accident".
				Neptune.loadSplashes();
				
				// Grab the RNG from the main menu.  One could probably be made, but
				// I felt that this one would work better.
				Field random = GuiMainMenu.class.getDeclaredFields()[1];
				random.setAccessible(true);
				Random r = (Random) random.get(ev.gui);
				
				// And now hijack the splash text with reflection.
				Field splashText = GuiMainMenu.class.getDeclaredFields()[3];
				splashText.setAccessible(true);
				splashText.set(ev.gui, chooseSplash(r));
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
