![by-sa.png](https://bitbucket.org/repo/xnzLXA/images/2501625781-by-sa.png)

The Neptune Splash API is licensed under the [Creative Commons Attribution Share-Alike 4.0](http://creativecommons.org/licenses/by-sa/4.0/).